/*global require:false, console:false*/


// DECLARAÇÃO DE DEPENDENCIAS


var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    shell        = require('gulp-shell'),
    concat       = require('gulp-concat'),
    include      = require('gulp-html-tag-include'),
    htmlmin      = require('gulp-htmlmin'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS     = require('gulp-clean-css'),
    inject       = require('gulp-inject'),
    browserify   = require('gulp-browserify'),
    uglify       = require('gulp-uglify'),
    jshint       = require('gulp-jshint'),
    imagemin     = require('gulp-imagemin');


// TAREFAS DO AMBIENTE DE DESENVOLVIMENTO


// TAREFAS PARA COPIAR TODAS AS FONTES DO PROJETO
gulp.task('copy-fonts', function () {
    'use strict';
    return gulp.src('bower_components/font-awesome/fonts/**')
               .pipe(gulp.dest('./app/dist/fonts'));
});


// TAREFAS PARA COPIAR TODAS AS IMAGES DO PROJETO
gulp.task('copy-images', function () {
    'use strict';
    return gulp.src([
        './app/src/images/**/*.png',
        './app/src/images/**/*.jpg',
        './app/src/images/**/*.gif',
        './app/src/images/**/*.jpeg'
    ])
               .pipe(gulp.dest('./app/dist/images'));
});


// TAREFA PARA COPIAR EM UM UNICO ARQUIVO TODOS OS ESTILOS EXTERNOS
gulp.task('copy-vendor-css', function () {
    'use strict';
    return gulp.src([
        'bower_components/font-awesome/css/font-awesome.css'
    ])
               .pipe(concat('vendor.css'))
               .pipe(gulp.dest('./app/dist/css'));
});


// TAREFAS PARA COPIAR EM UM UNICO ARQUIVO TODOS OS SCRIPTS EXTERNOS
gulp.task('copy-vendor-js', function () {
    'use strict';
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js'
    ])
               .pipe(concat('vendor.js'))
               .pipe(gulp.dest('./app/dist/js'));
});


// TAREFA PARA TRANSFORMAR TODOS ESTILOS SASS EM CSS
gulp.task('sass', function () {
    'use strict';
    return gulp.src('./app/src/scss/*.scss')
               .pipe(sass().on('error', sass.logError))
               .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
               .pipe(gulp.dest('./app/dist/css'))
               .pipe(browserSync.stream());
});


// TAREFA PARA TRATAR TODOS OS SCRIPTS
gulp.task('js', function () {
    'use strict';
    return gulp.src('./app/src/js/*.js')
               .pipe(jshint())
               .pipe(jshint.reporter('default'))
               .pipe(browserify())
               .pipe(gulp.dest('./app/dist/js'));
});


// TAREFA PARA TRATAR O LIVE RELOAD APOS ALTERACAO DE UM SCRIPT
gulp.task('js-watch', ['js'], function (done) {
    'use strict';
    browserSync.reload();
    done();
});


// TAREFA PARA TRATAR INCLUDES ( HEADER E FOOTER )
gulp.task('html-include', function () {
    'use strict';
    return gulp.src('./app/*.html')
               .pipe(include())
               .pipe(gulp.dest('./app/dist'));
});


// TAREFA PARA TRATAR AS CHAMADAS DOS ESTILOS E SCRIPTS
gulp.task('inject', function () {
    'use strict';
    return gulp.src('./app/dist/*.html')
               .pipe(inject(gulp.src(['./app/dist/js/vendor.js',
               './app/dist/js/core.js',
               './app/dist/css/vendor.css',
               './app/dist/css/core.css'],
               {read: false}), {relative: true}))
               .pipe(gulp.dest('./app/dist'))
               .pipe(browserSync.stream());
});


// TAREFA PARA TRATAR O LIVE RELOAD DE ACORDO COM AS ALTERACOES NOS ARQUIVOS DO AMBIENTE DE DESENVOLVIMENTO
gulp.task('development', function () {
    'use strict';
    browserSync.init({
        server: './app/dist'
    });

    gulp.watch('./app/src/scss/**/*.scss', ['sass']);
    gulp.watch('./app/src/js/*.js', ['js-watch']);
    gulp.watch('./app/**/*.html', ['html-include']);
    gulp.watch('./app/dist/**/*.html', ['inject']);
});


// TAREFAS DO AMBIENTE DE PRODUCAO


// TAREFA PARA MINIFICAR TODOS OS ESTILOS
gulp.task('css-build', function () {
    'use strict';
    return gulp.src([
        './app/dist/css/vendor.css',
        './app/dist/css/core.css'
    ])
               .pipe(concat('core.min.css'))
               .pipe(cleanCSS({compatibility: 'ie8'}))
               .pipe(gulp.dest('./app/dist/css'));
});


// TAREFA PARA MINIFICAR TODOS OS SCRIPTS
gulp.task('js-build', function () {
    'use strict';
    return gulp.src([
        './app/dist/js/vendor.js',
        './app/dist/js/core.js'
    ])
               .pipe(concat('core.min.js'))
               .pipe(uglify())
               .pipe(gulp.dest('./app/dist/js'));
});


// TAREFA PARA TRATAR AS CHAMADAS DOS ESTILOS E SCRIPTS
gulp.task('inject-prod', function () {
    'use strict';
    return gulp.src('./app/dist/index.html')
               .pipe(inject(gulp.src(['./app/dist/js/*.js', './app/dist/css/*.css'], {read: false}), {relative: true, removeTags: true}))
               .pipe(gulp.dest('./app/dist'));
});


// TAREFA PARA MINIFICAR O HTML
gulp.task('minify', function () {
    'use strict';
    return gulp.src('./app/dist/*.html')
               .pipe(htmlmin({collapseWhitespace: true}))
               .pipe(gulp.dest('./app/dist'));
});


// TAREFA PARA OTIMIZAR TODAS AS IMAGENS
gulp.task('images', function () {
    'use strict';
    return gulp.src('./app/src/images/**/*')
               .pipe(imagemin([ imagemin.gifsicle({interlaced: true}),
                                imagemin.jpegtran({progressive: true}),
                                imagemin.optipng({optimizationLevel: 5}),
                                imagemin.svgo({plugins: [{removeViewBox: true}]})
                            ]))
               .pipe(gulp.dest('./app/dist/images'));
});


// TAREFA PARA SUBIR O SERVIDOR COM OS ARQUIVOS DO BUILD DE PRODUCAO
gulp.task('production',  function () {
    'use strict';
    browserSync.init({
        server: './app/dist',
        port: '4000'
    });
});


// TAREFAS PARA TRATAR O AMBIENTE DE DESENVOLVIMENTO
gulp.task('clean', shell.task('rm -rf node_modules bower_components && npm cache clean && npm install && bower install'));
gulp.task('dist', shell.task('rm -rf app/dist'));
gulp.task('remove-assets', shell.task('rm -rf app/dist/css/vendor.css app/dist/css/core.css app/dist/js/vendor.js app/dist/js/core.js'));


// TAREFAS PARA SUBIR O SERVIDOR DE DESENVOLVIMENTO
gulp.task('copy-files', ['copy-fonts', 'copy-images', 'copy-vendor-css', 'copy-vendor-js']);
gulp.task('assets', ['sass', 'js']);
gulp.task('dev', shell.task('gulp dist && gulp copy-files && gulp assets && gulp html-include && gulp inject && gulp development'));


// TAREFA PARA BUILD DE PRODUCAO
gulp.task('assets-prod', ['css-build', 'js-build']);
gulp.task('prod', shell.task('gulp assets-prod && gulp remove-assets && gulp inject-prod && gulp minify && gulp images && gulp production'));

# Web Starter Kit #

Modelo de projeto

### Requisitos ###

* [Gulp](http://gulpjs.com/)
* [Bower](https://bower.io/)

### Árvore de diretórios ###

```sh
projeto/
| - app/
| - | - includes/
| - | - | - footer.js
| - | - | - header.js
| - | - src/
| - | - | - images/
| - | - | - | - lorempixel.jpg
| - | - | - js/
| - | - | - | - core.js
| - | - | - scss/
| - | - | - | - partials/   
| - | - | - | - | - _global.scss
| - | - | - | - | - _home.scss
| - | - | - | - | - _variables.scss
| - | - | - | - core.scss
| - | - index.html
| - bower.json
| - gulpfile.js
| - package.json
```

### Instalação ###

* Instalar dependências

```sh
$ npm install && bower install
```

* Subir servidor de desenvolvimento

```sh
$ gulp dev
```

* Subir servidor de produção

```sh
$ gulp prod
```

### Comandos ( Extra ) ###

* Reinstalar dependências

```sh
$ gulp clean
```
